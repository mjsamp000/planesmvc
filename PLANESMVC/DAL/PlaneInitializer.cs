﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PLANESMVC.Models;

namespace PLANESMVC.DAL
{
    public class PlaneInitializer : DropCreateDatabaseIfModelChanges<PlaneContext>
    {
        protected override void Seed(PlaneContext context)
        {
            // Initalize database

            // List of Admin
            var admin = new List<Person>
            {
                new Person { FirstName="Mark",LastName="De Guzman", Username = "admin1", Password = "admin1", Active = true, IsAdmin = true },
                new Person { FirstName="Timothy",LastName="Valera", Username = "admin2", Password = "admin2", Active = true, IsAdmin = true },
                new Person { FirstName="Like",LastName="Diaz", Username = "admin3", Password = "admin3", Active = true, IsAdmin = true }
            };
            admin.ForEach(s => context.People.Add(s));
            context.SaveChanges();

            // List of Students
            var students = new List<Student>
            {
                new Student { FirstName="Carson",LastName="Alexander", Username = "stud1", Password = "stud1", Active = true },
                new Student { FirstName="Meredith",LastName="Alonso", Username = "stud2", Password = "stud2", Active = true },
                new Student { FirstName="Arturo",LastName="Anand", Username = "stud3", Password = "stud3", Active = true },
                new Student { FirstName="Gytis",LastName="Barzdukas", Username = "stud4", Password = "stud4", Active = true },
                new Student { FirstName="Yan",LastName="Li", Username = "stud5", Password = "stud5", Active = true }
            };
            students.ForEach(s => context.People.Add(s));
            context.SaveChanges();

            // List of Tutors
            var tutors = new List<Tutor>
            {
                new Tutor { FirstName="Peggy",LastName="Justice", Username = "tut1", Password = "tut1", Active = true },
                new Tutor { FirstName="Laura",LastName="Norman", Username = "tut2", Password = "tut2", Active = true },
                new Tutor { FirstName="Nino",LastName="Olivetto", Username = "tut3", Password = "tut3", Active = true }
            };
            tutors.ForEach(s => context.People.Add(s));
            context.SaveChanges();

            // List of Planes
            var planes = new List<Plane>
            {
                new Plane { Name = "Aircraft Carrier", Code = "BP96243", Active = true },
                new Plane { Name = "Herpa Airbus House A350 1/500 Carbon", Code = "HE528801-001", Active = true },
                new Plane { Name = "Skymarks Airbus A380-800 H/C", Code = "SKR380",Active = true },
                new Plane { Name = "Gemini Lan Chile", Code = "GJ228", Active = true },
                new Plane { Name = "Skymarks Jetblue E190 1/100 Blueberries", Code = "SKR851", Active = false },

            };
            planes.ForEach(s => context.Planes.Add(s));
            context.SaveChanges();

            // List of Airports
            var airports = new List<Airport>
            {
                new Airport { Name = "Airport 123", Active = true },
                new Airport { Name = "Airport 456", Active = true },
                new Airport { Name = "Airport 789", Active = false },
                new Airport { Name = "Airport 012", Active = true },
                new Airport { Name = "Airport 345", Active = true }

            };
            airports.ForEach(s => context.Airports.Add(s));
            context.SaveChanges();
        }
    }
}