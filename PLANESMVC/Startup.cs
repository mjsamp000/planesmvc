﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PLANESMVC.Startup))]
namespace PLANESMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
