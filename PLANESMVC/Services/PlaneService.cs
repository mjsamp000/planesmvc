﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using LinqKit;
using PLANESMVC.DAL;
using PLANESMVC.Models;
using PLANESMVC.Models.ViewModels;

namespace PLANESMVC.Services
{
    public class PlaneService
    {
        // get all planes
        public static ServiceResultPlanes GetPlanes(PlaneFilter filter = null)
        {
            ServiceResultPlanes result = new ServiceResultPlanes();

            using (var db = new PlaneContext())
            {
                var predicate = PredicateBuilder.New<Plane>(true);

                if (filter != null)
                {
                    #region filter settings
                    if (!(string.IsNullOrEmpty(filter.Name)))
                    {
                        predicate = predicate.Or(r => r.Name.Contains(filter.Name));
                    }
                    if (!(string.IsNullOrEmpty(filter.Code)))
                    {
                        predicate = predicate.Or(r => r.Code.Contains(filter.Code));
                    }

                    if (filter.Active)
                    {
                        predicate = predicate.Or(r => r.Active == true);
                    }
                    if (filter.Inactive)
                    {
                        predicate = predicate.Or(r => r.Active == false);
                    }
                    #endregion
                }

                var planes = db.Planes.AsExpandable().Where(predicate).ToList();


                result.Total = planes.Count();

                foreach (var p in planes)
                {
                    result.Payload.Add(Mapper.Map<PlaneViewModel>(p));
                }
            }

            return result;
        }

        // get plane by Id
        public static ServiceResultPlane GetPlaneById(int id)
        {
            ServiceResultPlane result = new ServiceResultPlane();

            using (var db = new PlaneContext())
            {
                var plane = db.Planes.FirstOrDefault(r => r.Id == id);

                if (plane != null)
                {
                    result.Payload = Mapper.Map<PlaneViewModel>(plane);
                }
            }

            return result;
        }

        // new plane
        public static ServiceResultNewPlane Create(PlaneViewModel model)
        {
            ServiceResultNewPlane result = new ServiceResultNewPlane();

            var plane = new Plane();

            using (var db = new PlaneContext())
            {
                plane = Mapper.Map<Plane>(model);

                db.Planes.Add(plane);
                db.SaveChanges();
            }

            result.Payload = plane.Id;

            return result;
        }

        // update plane
        public static ServiceResultPlane Update(PlaneViewModel model)
        {
            ServiceResultPlane result = new ServiceResultPlane();

            var plane = new Plane();

            using (var db = new PlaneContext())
            {
                plane = db.Planes.AsNoTracking().FirstOrDefault(r => r.Id == model.Id);
                if (plane != null)
                {
                    plane = Mapper.Map<Plane>(model);
                    db.Entry(plane).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }

            result.Payload = Mapper.Map<PlaneViewModel>(plane);

            return result;
        }
    }
}