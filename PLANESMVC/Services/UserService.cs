﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using LinqKit;
using PLANESMVC.DAL;
using PLANESMVC.Models;
using PLANESMVC.Models.ViewModels;

namespace PLANESMVC.Services
{
    public class UserService
    {
        // get all users
        public static ServiceResultUsers GetUsers(UserFilter filter = null)
        {
            ServiceResultUsers result = new ServiceResultUsers();

            using (var db = new PlaneContext())
            {
                var predicate = PredicateBuilder.New<Person>(true);

                if (filter != null)
                {
                    #region filter settings
                    if (!(string.IsNullOrEmpty(filter.FirstName)))
                    {
                        predicate = predicate.Or(r => r.FirstName.Contains(filter.FirstName));
                    }
                    if (!(string.IsNullOrEmpty(filter.LastName)))
                    {
                        predicate = predicate.Or(r => r.FirstName.Contains(filter.LastName));
                    }


                    if (filter.Admin)
                    {
                        predicate = predicate.Or(r => r.IsAdmin == true);
                    }
                    if (filter.Student)
                    {
                        predicate = predicate.Or(r => r.IsStudent == true);
                    }
                    if (filter.Tutor)
                    {
                        predicate = predicate.Or(r => r.IsTutor == true);
                    }


                    if (filter.Active)
                    {
                        predicate = predicate.Or(r => r.Active == true);
                    }
                    if (filter.Inactive)
                    {
                        predicate = predicate.Or(r => r.Active == false);
                    }
                    #endregion
                }

                var people = db.People.AsExpandable().Where(predicate).ToList();


                result.Total = people.Count();

                foreach (var p in people)
                {
                    result.Payload.Add(Mapper.Map<UserViewModel>(p));
                }
            }
            
            return result;
        }

        // get user by Id
        public static ServiceResultUser GetUserById(int id)
        {
            ServiceResultUser result = new ServiceResultUser();

            using (var db = new PlaneContext())
            {
                var person = db.People.FirstOrDefault(r => r.Id == id);

                if (person != null)
                {
                    result.Payload = Mapper.Map<UserViewModel>(person);
                }
            }

            return result;
        }

        // new user
        public static ServiceResultNewUser Create(UserViewModel model)
        {
            ServiceResultNewUser result = new ServiceResultNewUser();

            var person = new Person();

            using (var db = new PlaneContext())
            {
                person = Mapper.Map<Person>(model);

                db.People.Add(person);
                db.SaveChanges();
            }

            result.Payload = person.Id;

            return result;
        }

        // update user
        public static ServiceResultUser Update(UserViewModel model)
        {
            ServiceResultUser result = new ServiceResultUser();

            var person = new Person();

            using (var db = new PlaneContext())
            {
                person = db.People.AsNoTracking().FirstOrDefault(r => r.Id == model.Id);
                if (person != null)
                {
                    person = Mapper.Map<Person>(model);
                    db.Entry(person).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }

            result.Payload = Mapper.Map<UserViewModel>(person);

            return result;
        }
    }
}