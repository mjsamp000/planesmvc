﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using LinqKit;
using PLANESMVC.DAL;
using PLANESMVC.Models;
using PLANESMVC.Models.ViewModels;

namespace PLANESMVC.Services
{
    public class AirportService
    {
        // get all airports
        public static ServiceResultAirports GetAirports(AirportFilter filter = null)
        {
            ServiceResultAirports result = new ServiceResultAirports();

            using (var db = new PlaneContext())
            {
                var predicate = PredicateBuilder.New<Airport>(true);

                if (filter != null)
                {
                    #region filter settings
                    if (!(string.IsNullOrEmpty(filter.Name)))
                    {
                        predicate = predicate.Or(r => r.Name.Contains(filter.Name));
                    }

                    if (filter.Active)
                    {
                        predicate = predicate.Or(r => r.Active == true);
                    }
                    if (filter.Inactive)
                    {
                        predicate = predicate.Or(r => r.Active == false);
                    }
                    #endregion
                }

                var airports = db.Airports.AsExpandable().Where(predicate).ToList();


                result.Total = airports.Count();

                foreach (var p in airports)
                {
                    result.Payload.Add(Mapper.Map<AirportViewModel>(p));
                }
            }

            return result;
        }

        // get airport by Id
        public static ServiceResultAirport GetAirportById(int id)
        {
            ServiceResultAirport result = new ServiceResultAirport();

            using (var db = new PlaneContext())
            {
                var airport = db.Airports.FirstOrDefault(r => r.Id == id);

                if (airport != null)
                {
                    result.Payload = Mapper.Map<AirportViewModel>(airport);
                }
            }

            return result;
        }

        // new airport
        public static ServiceResultNewAirport Create(AirportViewModel model)
        {
            ServiceResultNewAirport result = new ServiceResultNewAirport();

            var airport = new Airport();

            using (var db = new PlaneContext())
            {
                airport = Mapper.Map<Airport>(model);

                db.Airports.Add(airport);
                db.SaveChanges();
            }

            result.Payload = airport.Id;

            return result;
        }

        // update airport
        public static ServiceResultAirport Update(AirportViewModel model)
        {
            ServiceResultAirport result = new ServiceResultAirport();

            var airport = new Airport();

            using (var db = new PlaneContext())
            {
                airport = db.Airports.AsNoTracking().FirstOrDefault(r => r.Id == model.Id);
                if (airport != null)
                {
                    airport = Mapper.Map<Airport>(model);
                    db.Entry(airport).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }

            result.Payload = Mapper.Map<AirportViewModel>(airport);

            return result;
        }
    }
}