﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PLANESMVC.Models;
using PLANESMVC.Models.ViewModels;

namespace PLANESMVC.Services
{
    public class ServiceResultUsers : ServiceResult
    {
        public new List<UserViewModel> Payload { get; set; }
        public int Total { get; set; }

        public ServiceResultUsers()
        {
            this.Payload = new List<UserViewModel>();
            this.Total = 0;
        }
    }

    public class ServiceResultUser : ServiceResult
    {
        public new UserViewModel Payload { get; set; }
    }

    public class ServiceResultNewUser : ServiceResult
    {
        public new int Payload { get; set; }
    }




    public class ServiceResultPlanes : ServiceResult
    {
        public new List<PlaneViewModel> Payload { get; set; }
        public int Total { get; set; }

        public ServiceResultPlanes()
        {
            this.Payload = new List<PlaneViewModel>();
            this.Total = 0;
        }
    }

    public class ServiceResultPlane : ServiceResult
    {
        public new PlaneViewModel Payload { get; set; }
    }

    public class ServiceResultNewPlane : ServiceResult
    {
        public new int Payload { get; set; }
    }




    public class ServiceResultAirports : ServiceResult
    {
        public new List<AirportViewModel> Payload { get; set; }
        public int Total { get; set; }

        public ServiceResultAirports()
        {
            this.Payload = new List<AirportViewModel>();
            this.Total = 0;
        }
    }

    public class ServiceResultAirport : ServiceResult
    {
        public new AirportViewModel Payload { get; set; }
    }

    public class ServiceResultNewAirport : ServiceResult
    {
        public new int Payload { get; set; }
    }




    public abstract class ServiceResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        protected object Payload { get; set; }

        public ServiceResult()
        {
            Success = true;
            Message = "Operation Completed Successfully";
        }
    }
}