﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PLANESMVC.Models;
using PLANESMVC.Models.ViewModels;

namespace PLANESMVC
{
    public class AutoMapperConfig
    {
        public static void Init()
        {
            // Map - Person
            Mapper.CreateMap<Person, UserViewModel>();
            Mapper.CreateMap<UserViewModel, Person>();

            // Map - Plane
            Mapper.CreateMap<Plane,PlaneViewModel>();
            Mapper.CreateMap<PlaneViewModel, Plane>();

            // Map - Airport
            Mapper.CreateMap<Airport, AirportViewModel>();
            Mapper.CreateMap<AirportViewModel, Airport>();
        }
    }
}