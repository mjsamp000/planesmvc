﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using PLANESMVC.Models.ViewModels;
using PLANESMVC.Services;

namespace PLANESMVC.Controllers
{
    public class AdminController : Controller
    {
        // GET: Users
        public ActionResult Users()
        {
            return View();
        }
        
        // POST: User
        [HttpPost]
        public ActionResult CreateUser(UserViewModel model)
        {
            var result = UserService.Create(model);
            
            return Content(JsonConvert.SerializeObject(result));
        }

        // POST: User
        [HttpPost]
        public ActionResult UpdateUser(UserViewModel model)
        {
            var result = UserService.Update(model);

            return Content(JsonConvert.SerializeObject(result));
        }

        // POST: User
        [HttpPost]
        public ActionResult StatusUpdateUser(UserViewModel model)
        {
            var result = UserService.Update(model);

            return Content(JsonConvert.SerializeObject(result));
        }




        // GET: Planes
        public ActionResult Planes()
        {
            return View();
        }

        // POST: Plane
        [HttpPost]
        public ActionResult CreatePlane(PlaneViewModel model)
        {
            var result = PlaneService.Create(model);

            return Content(JsonConvert.SerializeObject(result));
        }

        // POST: Plane
        [HttpPost]
        public ActionResult UpdatePlane(PlaneViewModel model)
        {
            var result = PlaneService.Update(model);

            return Content(JsonConvert.SerializeObject(result));
        }



        // GET: Airports
        public ActionResult Airports()
        {
            return View();
        }

        // POST: Airport
        [HttpPost]
        public ActionResult CreateAirport(AirportViewModel model)
        {
            var result = AirportService.Create(model);

            return Content(JsonConvert.SerializeObject(result));
        }

        // POST: Airport
        [HttpPost]
        public ActionResult UpdateAirport(AirportViewModel model)
        {
            var result = AirportService.Update(model);

            return Content(JsonConvert.SerializeObject(result));
        }





        // APIs
        [HttpPost]
        public ActionResult getUsers(UserFilter filter = null)
        {
            var result = UserService.GetUsers(filter);

            return Content(JsonConvert.SerializeObject(result));
        }

        public ActionResult getUser(int id)
        {
            var result = UserService.GetUserById(id);

            return Content(JsonConvert.SerializeObject(result));
        }



        [HttpPost]
        public ActionResult getPlanes(PlaneFilter filter = null)
        {
            var result = PlaneService.GetPlanes(filter);

            return Content(JsonConvert.SerializeObject(result));
        }

        public ActionResult getPlane(int id)
        {
            var result = PlaneService.GetPlaneById(id);

            return Content(JsonConvert.SerializeObject(result));
        }



        [HttpPost]
        public ActionResult getAirports(AirportFilter filter = null)
        {
            var result = AirportService.GetAirports(filter);

            return Content(JsonConvert.SerializeObject(result));
        }

        public ActionResult getAirport(int id)
        {
            var result = AirportService.GetAirportById(id);

            return Content(JsonConvert.SerializeObject(result));
        }

    }
}