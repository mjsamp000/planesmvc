﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PLANESMVC.Models
{
    public class Airport
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<Booking> Bookings { get; set; }
    }
}