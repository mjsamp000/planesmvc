﻿using System;
using System.Collections.Generic;

namespace PLANESMVC.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
        public bool IsStudent { get; set; }
        public bool IsTutor { get; set; }
        public bool IsAdmin { get; set; }
        
        public virtual ICollection<Booking> Bookings { get; set; }
    }
}