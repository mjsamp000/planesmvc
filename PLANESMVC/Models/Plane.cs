﻿using System;
using System.Collections.Generic;

namespace PLANESMVC.Models
{
    public class Plane
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }

        public virtual ICollection<Booking> Bookings { get; set; }
    }
}