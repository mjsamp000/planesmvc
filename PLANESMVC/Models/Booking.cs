﻿using System;
using System.Collections.Generic;

namespace PLANESMVC.Models
{
    public class Booking
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public int TutorId { get; set; }
        public int PlaneId { get; set; }
        public int AirportId { get; set; }
        public DateTime DateReserved { get; set; }
        public DateTime DateApproved { get; set; }
        public int ApprovedBy { get; set; }
        public DateTime DateCancelled { get; set; }
        public int CancelledBy { get; set; }
        public string Notes { get; set; }
    }
}