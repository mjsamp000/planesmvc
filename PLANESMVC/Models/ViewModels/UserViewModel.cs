﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PLANESMVC.Models.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Active { get; set; }
        public bool IsStudent { get; set; }
        public bool IsTutor { get; set; }
        public bool IsAdmin { get; set; }
        public string Type
        {
            get
            {
                if (IsAdmin) { return "Admin"; }
                if (IsStudent) { return "Student"; }
                if (IsTutor) { return "Tutor"; }

                return string.Empty;
            }
            set
            {
                if (value == "Admin") { IsAdmin = true; IsStudent = false; IsTutor = false; }
                if (value == "Student") { IsStudent = true; IsAdmin = false; IsTutor = false; }
                if (value == "Tutor") { IsTutor = true; IsAdmin = false; IsStudent = false; }
            }
        }
    }

    public class UserFilter
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Active { get; set; }
        public bool Inactive { get; set; }
        public bool Student { get; set; }
        public bool Tutor { get; set; }
        public bool Admin { get; set; }
    }
}