﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PLANESMVC.Models.ViewModels
{
    public class PlaneViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
    }

    public class PlaneFilter
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Active { get; set; }
        public bool Inactive { get; set; }
    }
}