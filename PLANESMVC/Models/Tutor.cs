﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PLANESMVC.Models
{
    public class Tutor : Person
    {
        public Tutor()
        {
            IsTutor = true;
        }
    }
}