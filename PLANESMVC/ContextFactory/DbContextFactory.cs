﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using PLANESMVC.DAL;

namespace PLANESMVC
{
    public static class DbContextFactory
    {
        static Func<PlaneContext> factory;

        public static void SetFactory(Func<PlaneContext> creationFactory)
        {
            factory = creationFactory;
        }

        public static PlaneContext CreateContext()
        {
            if (factory == null) throw new InvalidOperationException("You can not create a context without first building the factory.");

            return factory();
        }
    }
}